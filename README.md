# Tutorials, Fortune Cookies & Doctrine Queries

Well hi there! This repository holds the code and script
for the [Doctrine Queries Tutorials](https://symfonycasts.com/screencast/doctrine-queries) on SymfonyCasts.

## Setup

If you've just downloaded the code, congratulations!!

To get it working, follow these steps:

**Download Composer dependencies**

Make sure you have [Composer installed](https://getcomposer.org/download/)
and then run:

```
composer install
```

You may alternatively need to run `php composer.phar install`, depending
on how you installed Composer.

**Database Setup**

The code comes with a `docker-compose.yaml` file, and we recommend using
Docker to boot a database container. You will still have PHP installed
locally, but you'll connect to a database inside Docker. This is optional,
but I think you'll love it!

First, make sure you have [Docker installed](https://docs.docker.com/get-docker/)
and running. To start the container, run:

```
docker compose up -d
```

Next, build the database with:

```
# "symfony console" is equivalent to "bin/console"
# but it's aware of your database container
symfony console doctrine:database:create --if-not-exists
symfony console doctrine:schema:update --force
symfony console doctrine:fixtures:load
```

(If you get an error about "MySQL server has gone away", just wait
a few seconds and try again - the container is probably still booting).

If you do *not* want to use Docker, just make sure to start your own
database server and update the `DATABASE_URL` environment variable in
`.env` or `.env.local` before running the commands above.

**Start the Symfony web server**

You can use Nginx or Apache, but Symfony's local web server
works even better.

To install the Symfony local web server, follow
"Downloading the Symfony client" instructions found
here: https://symfony.com/download - you only need to do this
once on your system.

Then, to start the web server, open a terminal, move into the
project, and run:

```
symfony serve
```

(If this is your first time using this command, you may see an
error that you need to run `symfony server:ca:install` first).

Now check out the site at `https://localhost:8000`

Have fun!

**OPTIONAL: Webpack Encore Assets**

This app uses Webpack Encore for the CSS, JS and image files.
But, the built files are already included... so you don't need
to download or build anything if you don't want to!

But if you *do* want to play with the CSS/JS and build the
final files, no problem. Make sure you have [yarn](https://yarnpkg.com/lang/en/)
or `npm` installed (`npm` comes with Node) and then run:

```
yarn install
yarn encore dev --watch

# or
npm install
npm run watch
```

## Have Ideas, Feedback or an Issue?

If you have suggestions or questions, please feel free to
open an issue on this repository or comment on the course
itself. We're watching both :).

## Thanks!

And as always, thanks so much for your support and letting
us do what we love!

<3 Your friends at SymfonyCasts
___
# Coding
___

## 1. Doctrine DQL
- When we need a custom query we can create custom repository methods to centralize everything.
- Doctrine uses its own query language `DQL`. AN example:
```php
[CategoryRepository.php]
/**
 * @return Category[]
 */
public function findByOrdered(): array
{
    $dql = 'SELECT category FROM App\Entity\Category as category ORDER BY category.name DESC';
    
    $query = $this->getEntityManager()->createQuery($dql);
    
    return $query->getResult();
}
```
- To see the RAW SQL You can type `$query->getSQL()`. Or inside a profiler.
- By the way, we normally don't write DQL by hand. Instead, we build it with the `QueryBuilder`.

## 2. The QueryBuilder
- Rewrite this method using `QueryBuilder`:
```php
[CategoryRepository.php]
/**
 * @return Category[]
 */
public function findByOrdered(): array
{
    $qb = $this->createQueryBuilder('category')
        ->orderBy('category.name', Criteria::DESC);

    $query = $qb->getQuery();

    return $query->getResult();
}
```
- To see RAW DQL you can type `$query->getDQL()`.

## 3. andWhere() and orWhere()
- For the `WHERE` clause, use `->andWhere()`. There is also a `where()` method... but I don't think I've ever used it! And... you shouldn't either. Using `andWhere()` is always ok - even if this is the first `WHERE` clause... and we don't really need the "and" part. Doctrine is smart enough to figure that out.
```php
[CategoryRepository.php]
/**
 * @return Category[]
 */
public function search(string $term): array
{
    return $this->createQueryBuilder('category')
        ->andWhere('category.name LIKE :searchTerm OR category.iconKey LIKE :searchTerm')
        ->setParameter('searchTerm', '%'.$term.'%')
        ->addOrderBy('category.name', 'DESC')
        ->getQuery()
        ->getResult();
}
```
- !!! Always use `->setParameter()` in chain to avoid SQL Injections.

## 4. JOINs
- Search in another table with this query... We need to JOIN. To be clear - `->leftJoin` because we're joining from one category to many fortune cookies.
```php
[CategoryRepository.php]
public function search(string $term): array
{
    return $this->createQueryBuilder('category')
        ->leftJoin('category.fortuneCookies', 'fortuneCookie')
        ->andWhere('category.name LIKE :searchTerm OR category.iconKey LIKE :searchTerm OR fortuneCookie.fortune LIKE :searchTerm')
        ->setParameter('searchTerm', '%'.$term.'%')
        ->addOrderBy('category.name', 'DESC')
        ->getQuery()
        ->getResult();
}
```

## 5. JOINs and addSelect Reduce Queries (N+1 problem)
- We need just select needed `fortuneCookie` data from `JOIN` just by adding `->addSelect()` to `QueryBuilder`
```php
[CategoryRepository.php]
public function search(string $term): array
{
    return $this->createQueryBuilder('category')
        ->addSelect('fortuneCookie')
        ->leftJoin('category.fortuneCookies', 'fortuneCookie')
        ->andWhere('category.name LIKE :searchTerm OR category.iconKey LIKE :searchTerm OR fortuneCookie.fortune LIKE :searchTerm')
        ->setParameter('searchTerm', '%'.$term.'%')
        ->addOrderBy('category.name', 'DESC')
        ->getQuery()
        ->getResult();
}
```

## 6. EXTRA_LAZY Relationships
- If you just count objects like we do in `homepage.html.twig`: `{{ category.fortuneCookies|length }}`, you can make the Doctrine to query just `count(*)` instead of querying all the data:
- Add `EXTRA_LAZY fetch` to `OneToMany` relation:
```php
[Category.php]
#[ORM\OneToMany(mappedBy: 'category', targetEntity: FortuneCookie::class, fetch: 'EXTRA_LAZY')]
    private Collection $fortuneCookies;
```
- One more place to fix N+1 problem is `showCategory()` method in `FortuneController.php`.
- Let's make `findWithFortunesJoin()` method in `CategoryRepository.php`:
```php
[CategoryRepository.php]
public function findWithFortunesJoin(int $id): ?Category
{
    return $this->createQueryBuilder('category')
        ->addSelect('fortuneCookie') // we are selecting fortuneCookie for this category
        ->leftJoin('category.fortuneCookies', 'fortuneCookie') // joining the fortuneCookie for category
        ->andWhere('category.id = :id')
        ->setParameter('id', $id)
        ->getQuery()
        ->getOneOrNullResult();
}
```
- And use it in `FortuneController.php`:
```php
[FortuneController.php]

#[Route('/category/{id}', name: 'app_category_show')]
public function showCategory(int $id, CategoryRepository $categoryRepository): Response
{
    $category = $categoryRepository->findWithFortunesJoin($id);

    if (!$category) {
        throw $this->createNotFoundException('Category not found');
    }

    return $this->render('fortune/showCategory.html.twig',[
        'category' => $category
    ]);
}
```
- If you have the N+1 problem, you can solve it by JOINing to the related table and selecting its data.
- But if you count and then access to related objects data, the `EXTRA_LAZY` feature will work against you. Doctrine will make 3 queries instead of 2.
```
1. Query the Category
2. Query the count of fortuneCookies
3. Query to get all the data from fortuneCookies
```

## 7. SELECT the SUM (or COUNT)
**Let's print the total number printed for all fortunes in this category on the category page.**
- Make new method in `FortuneCookieRepository.php`:
```php
[FortuneCookieRepository.php]
public function countNumberPrintedForCategory(Category $category): int
{
    $result = $this->createQueryBuilder('fortuneCookie')
        ->select('SUM(fortuneCookie.numberPrinted) AS fortunesPrinted')
        ->andwhere('fortuneCookie.category = :category')
        ->setParameter('category', $category)
        ->getQuery()
        ->getSingleScalarResult();

    return (int) $result;
}
```
- Use it in `FortuneController.php`:
```php
[FortuneController.php]
#[Route('/category/{id}', name: 'app_category_show')]
public function showCategory(int $id, CategoryRepository $categoryRepository, FortuneCookieRepository $fortuneCookieRepository): Response
{
    $category = $categoryRepository->findWithFortunesJoin($id);

    if (!$category) {
        throw $this->createNotFoundException('Category not found');
    }

    $fortunesPrinted = $fortuneCookieRepository->countNumberPrintedForCategory($category);

    return $this->render('fortune/showCategory.html.twig',[
        'category' => $category,
        'fortunesPrinted' => $fortunesPrinted
    ]);
}
```
- Use it in `showCategory.html.twig`
```html
[showCategory.html.twig]

...

<th class="border p-4">
    Print History ({{ fortunesPrinted|number_format }} total)
</th>

...
```

## 8. Selecting Specific Fields
How about the average number of fortune cookies printed for this category?
- modify the `countNumberPrintedForCategory()` method in `FortuneCookieRepository.php`:
```php
[FortuneCookieRepository.php]
public function countNumberPrintedForCategory(Category $category): array
{
    $result = $this->createQueryBuilder('fortuneCookie')
        ->select('SUM(fortuneCookie.numberPrinted) AS fortunesPrinted')
        ->addSelect('AVG(fortuneCookie.numberPrinted) fortunesAverage')
        ->addSelect('category.name')
        ->innerJoin('fortuneCookie.category', 'category')
        ->andwhere('fortuneCookie.category = :category')
        ->setParameter('category', $category)
        ->getQuery()
        ->getSingleResult();

    return $result;
}
```
- Tweak the `showCategory()` action in `FortuneController.php` to pass new data to the `showCategory.html.twig` template:
```php
[FortuneController.php]
#[Route('/category/{id}', name: 'app_category_show')]
public function showCategory(int $id, CategoryRepository $categoryRepository, FortuneCookieRepository $fortuneCookieRepository): Response
{
    $category = $categoryRepository->findWithFortunesJoin($id);

    if (!$category) {
        throw $this->createNotFoundException('Category not found');
    }

    $result = $fortuneCookieRepository->countNumberPrintedForCategory($category);

    return $this->render('fortune/showCategory.html.twig',[
        'category' => $category,
        'fortunesPrinted' => $result['fortunesPrinted'],
        'fortunesAverage' => $result['fortunesAverage'],
        'categoryName' => $result['name'],
    ]);
}
```
- Use it in `showCategory.html.twig` template:
```html
[showCategory.html.twig]

...

<th class="border p-4">
    Print History ({{ fortunesPrinted|number_format }} total
    {{ fortunesAverage|number_format }} average)
</th>

...
```
**Getting full entity objects back from Doctrine is the ideal situation because... objects are just really nice to work with.**

But at the end of the day, if you need to query for specific data or columns, you can totally do that. And as we just saw, Doctrine will return an associative array.

## 9. SELECTing into a New DTO Object
So, what if we convert this `$result` array into object? 
- First, we need to create a new class that will hold the data from our query `Model/CategoryFortuneStats.php`.
```php
class CategoryFortuneStats
{
    public function __construct(
        public int $fortunesPrinted,
        public float $fortunesAverage,
        public string $categoryName,
    )
    {
    }
}
```
- Then change the `countNumberPrintedForCategory` method in `FortuneCookieRepository.php`:
```php
[FortuneCookieRepository.php]
public function countNumberPrintedForCategory(Category $category): CategoryFortuneStats
{
    $result = $this->createQueryBuilder('fortuneCookie')
        ->select(sprintf(
            'NEW %s(
                SUM(fortuneCookie.numberPrinted),
                AVG(fortuneCookie.numberPrinted),
                category.name
            )',
            CategoryFortuneStats::class
        ))
        ->innerJoin('fortuneCookie.category', 'category')
        ->andwhere('fortuneCookie.category = :category')
        ->setParameter('category', $category)
        ->getQuery()
        ->getSingleResult();

    return $result;
}
```
- Next change the properties passed to `showCategory.html.twig` in `FortuneController.php`:
```php
[FortuneController.php]

#[Route('/category/{id}', name: 'app_category_show')]
public function showCategory(int $id, CategoryRepository $categoryRepository, FortuneCookieRepository $fortuneCookieRepository): Response
{
    $category = $categoryRepository->findWithFortunesJoin($id);

    if (!$category) {
        throw $this->createNotFoundException('Category not found');
    }

    $stats = $fortuneCookieRepository->countNumberPrintedForCategory($category);

    return $this->render('fortune/showCategory.html.twig',[
        'category' => $category,
        'fortunesPrinted' => $stats->fortunesPrinted,
        'fortunesAverage' => $stats->fortunesAverage,
        'categoryName' => $stats->categoryName,
    ]);
}
```

## 10. Raw SQL Queries
To do RAW SQL queries we need to use LowLevel **DBAL** (Database Abstraction Library) that integrated into Doctrine. Instead of HighLevel **ORM** (Object Relational Mapper).
- Replace all the `$result` and the `return` in the `countNumberPrintedForCategory()` method:
```php
[FortuneCookieRepository.php]
public function countNumberPrintedForCategory(Category $category): CategoryFortuneStats
{
    $conn = $this->getEntityManager()->getConnection();
    $sql = 'SELECT SUM(fortune_cookie.number_printed) AS fortunesPrinted, AVG(fortune_cookie.number_printed) fortunesAverage, category.name AS categoryName FROM fortune_cookie INNER JOIN category ON category.id = fortune_cookie.category_id WHERE fortune_cookie.category_id = :category';
    $stmt = $conn->prepare($sql);
    $stmt->bindValue('category', $category->getId());
    $result = $stmt->executeQuery(
//            ['category' => $category->getId(),]
    );

    return new CategoryFortuneStats(...$result->fetchAssociative()); // ... - is the spread operator that  Array keys/values to parameters names/values
}
```

## 11. Reusing Queries in the Query Builder
- Refactor the `Category repository.php`:
```php
[Category repository.php]
/**
 * @return Category[]
 */
public function search(string $term): array
{
    $qb = $this->addOrderByCategoryName();

    return $this->addFortuneCookieJoinAndSelect($qb)
        ->andWhere('category.name LIKE :searchTerm OR category.iconKey LIKE :searchTerm OR fortuneCookie.fortune LIKE :searchTerm')
        ->setParameter('searchTerm', '%'.$term.'%')
        ->getQuery()
        ->getResult();
}

public function findWithFortunesJoin(int $id): ?Category
{
    return $this->addFortuneCookieJoinAndSelect()
        ->andWhere('category.id = :id')
        ->setParameter('id', $id)
        ->getQuery()
        ->getOneOrNullResult();
}

private function addFortuneCookieJoinAndSelect(QueryBuilder $qb = null): QueryBuilder
{
    return ($qb ?? $this->createQueryBuilder('category'))
        ->addSelect('fortuneCookie')
        ->leftJoin('category.fortuneCookies', 'fortuneCookie');
}

private function addOrderByCategoryName(QueryBuilder $qb = null): QueryBuilder
{
    return ($qb ?? $this->createQueryBuilder('category'))
        ->addOrderBy('category.name', 'DESC');
}
```
## 12. Criteria: Filter Relation Collections
Removing discontinued cookies from result:
- Create new `getFortuneCookiesStillInProduction()` method in `Category.php` with `Criteria`:
```php
[Category.php]
/**
 * @return Collection<int, FortuneCookie>
 */
public function getFortuneCookiesStillInProduction(): Collection
{
    $criteria = Criteria::create()
        ->andWhere(Criteria::expr()->eq('discontinued', false));

    return $this->fortuneCookies->matching($criteria);
}
```
- To be organized it better to keep all the query logic inside the `Repository` not in the middle of an `Entity`. Let's move!
- Make new `createFortuneCookiesStillInProductionCriteria()` method in `FortuneCookieRepository.php`:
```php
[FortuneCookieRepository.php]
public static function createFortuneCookiesStillInProductionCriteria(): Criteria
{
    return Criteria::create()
        ->andWhere(Criteria::expr()->eq('discontinued', false));
}
```
- Then modify `getFortuneCookiesStillInProduction()` method in `Category.php` to use `Criteria` from `FortuneCookieRepository.php`:
```php
[Category.php]
/**
 * @return Collection<int, FortuneCookie>
 */
public function getFortuneCookiesStillInProduction(): Collection
{
    $criteria = FortuneCookieRepository::createFortuneCookiesStillInProductionCriteria();
    
    return $this->fortuneCookies->matching($criteria);
}
```
- And lastly, use it in `.html.twig` templates:
```html
[homepage.html.twig]
{{ category.fortuneCookiesStillInProduction|length }}
```
```html
[showCategory.html.twig]
{% for fortuneCookie in category.fortuneCookiesStillInProduction %}
    ...
{% endfor %}
```

## 13. Filters: Automatically Modify Queries
If we want to apply some criteria globally to every query to a table, telling Doctrine that whenever we query for fortune cookies, we want to add a WHERE discontinued = false to that query. It's totally possible.
- Add new Filter class `src/Doctrine/DiscontinuedFilter.php`:
```php
[DiscontinuedFilter.php]
class DiscontinuedFilter extends SQLFilter
{

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {
        if ($targetEntity->name !== FortuneCookie::class) {
            return '';
        }

        return sprintf('%s.discontinued = false', $targetTableAlias);
    }
}
```
- To activate this filter class go to `config/packages/doctrine.yaml` file and tell Doctrine that the filter exists:
```yaml
[doctrine.yaml]
orm:
  ...
  filters:
    fortuneCookie_discontinued: App\Doctrine\DiscontinuedFilter
```
`fortuneCookie_discontinued` name can be any you want.
- Autowire `EntityManagerInterface $entityManager` in `FortuneController.php` in `index` action and use it:
```php
[FortuneController.php]
#[Route('/', name: 'app_homepage')]
public function index(Request $request, CategoryRepository $categoryRepository, EntityManagerInterface $entityManager): Response
{
    $entityManager->getFilters()
        ->enable('fortuneCookie_discontinued');
    
    ...
}
```
One thing about these filters is that they are not services. So you can't have a constructor... it's just not allowed. If we need to pass something to this - like some config - we have to do it a different way.

For example, let's pretend that sometimes we want to hide discontinued cookies... but other times, we want to show only discontinued ones - the reverse. Essentially, we want to be able to toggle this value from false to true.

- Change `false` to `%s`:
```php
[Doctrine/DiscontinuedFilter.php]
 public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
{
    ...
    
    return sprintf('%s.discontinued = %s', $targetTableAlias, $this->getParameter('discontinued'));
}
```
- And pass this parameter where we call this method:
```php
[FortunesController.php]
public function index(Request $request, CategoryRepository $categoryRepository, EntityManagerInterface $entityManager): Response
{
    $entityManager->getFilters()
        ->enable('fortuneCookie_discontinued')
        ->setParameter('discontinued', true);
    
    ...
}
```
- To enable this filter globally comment this new code in `index()` action below.
```php
[FortunesController.php]
public function index(Request $request, CategoryRepository $categoryRepository, EntityManagerInterface $entityManager): Response
{
//    $entityManager->getFilters()
//        ->enable('fortuneCookie_discontinued')
//        ->setParameter('discontinued', true);
    
    ...
}
```
- In `doctrine.yaml` make your filter like this:
```yaml
orm:
    ...
    filters:
        fortuneCookie_discontinued:
            class: App\Doctrine\DiscontinuedFilter
            enabled: true
            parameters:
              discontinued: false
```
This will enable your filter everywhere.

## 14. WHERE IN()
Let's make our search smarter to see if we can match categories by searching word by word.

## 15. Using RAND() or Other Non-Supported Functions
For the heck of it, let's randomize the order of the fortunes on a page.
- Change `find()` to `findWithFortunesJoin()` in `showCategory()` method in `FortuneController.php`:
```php
[FortuneController.php]
#[Route('/category/{id}', name: 'app_category_show')]
public function showCategory(int $id, CategoryRepository $categoryRepository, FortuneCookieRepository $fortuneCookieRepository): Response
{
    $category = $categoryRepository->findWithFortunesJoin($id);

    ...
}
```
- Then add `RAND()` order in `findWithFortunesJoin()` method in `CategoryRepository.php`:
```php
[CategoryRepository.php]
public function findWithFortunesJoin(int $id): ?Category
{
    return $this->addFortuneCookieJoinAndSelect()
        ->andWhere('category.id = :id')
        ->setParameter('id', $id)
        ->orderBy('RAND()', Criteria::ASC)
        ->getQuery()
        ->getOneOrNullResult();
}
```
Now we have an error `Error: Expected known function, got 'RAND'`.
- Install `beberlei/DoctrineExtensions` library: `composer require beberlei/doctrineextensions`.
- Then in `doctrine.yaml` config add this code:
```yaml
    orm:
        ...
        dql:
            numeric_functions:
                rand: DoctrineExtensions\Query\Mysql\Rand
```
Done!

## 16. Using GROUP BY to Fetch & Count in 1 Query
On the homepage we have 7 queries. This is... probably not a problem... and you shouldn't worry about optimizing performance until you actually see that there is a problem.

But let's challenge ourselves to turn these seven queries into one.
- Change `QueryBuilder` in `findByOrdered()` method in `CategoryRepository.php`
```php
[CategoryRepository.php]
public function findByOrdered(): array
{
    $qb = $this->createQueryBuilder('category')
        ->orderBy('category.name', Criteria::DESC)
        ->addSelect('COUNT(fortuneCookie.id) AS fortuneCookiesTotal')
        ->leftJoin('category.fortuneCookies', 'fortuneCookie')
        // for this COUNT work correctly we need to add GROUP BY
        ->addGroupBy('category.id');

    $query = $qb->getQuery();

    return $query->getResult();
}
```
But now this method doesn't return an `array` of `Category` objects...
- Move this code to the new method `addGroupByCategoryAndCountFortunes()`:
```php
[CategoryRepository.php]
private function addGroupByCategoryAndCountFortunes(QueryBuilder $qb = null): QueryBuilder
{
    return ($qb ?? $this->createQueryBuilder('category'))
        ->addSelect('COUNT(fortuneCookie.id) AS fortuneCookiesTotal')
        ->leftJoin('category.fortuneCookies', 'fortuneCookie')
        // for this COUNT work correctly we need to add GROUP BY
        ->addGroupBy('category.id');
    
}
```
- And replace in `findByOrdered()` method.
- Then modify `search()` method for searching:
```php
[CategoryRepository.php]
public function search(string $term): array
{
    $termList = explode(' ', $term);

    $qb = $this->addOrderByCategoryName();

    return $this->addGroupByCategoryAndCountFortunes($qb)
        ->andWhere('category.name LIKE :searchTerm OR category.name IN (:termList) OR category.iconKey LIKE :searchTerm OR fortuneCookie.fortune LIKE :searchTerm')
        ->setParameter('searchTerm', '%'.$term.'%')
        ->setParameter('termList', $termList)
        ->getQuery()
        ->getResult();
}
```
